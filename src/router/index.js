import Vue from 'vue'
import Router from 'vue-router'

const Login = () => import('@/views/login/Login.vue')
const Home = () => import('@/components/home/Homepage.vue')
const Employee = () => import('@/views/employee/Employee.vue')
const Department = () => import('@/views/department/Department.vue')
const AddEmployee = () => import('@/views/employee/AddEmployee.vue')
const Permission = () => import('@/views/permission/Permission.vue')
const Role = () => import('@/views/role/Role.vue')
const AddRole = () => import('@/views/role/AddRole.vue')
const SystemDictionary = () => import('@/views/systemDictionary/SystemDictionary.vue')
const SystemDictionaryItem = () => import('@/views/systemDictionary/SystemDictionaryItem.vue')
const CustomerLists = () => import('@/views/customer/CustomerLists.vue')
const PCustomerLists = () => import('@/views/customer/PotentialCustomer.vue')
const CustomerTraceHistory = () => import('@/views/customerTrace/CustomerTraceHistory.vue')
const RPoolCustomer = () => import('@/views/customer/RPoolCustomer.vue')
const FailCustomer = () => import('@/views/customer/FailCustomer.vue')
const CustomerReport = () => import('@/views/report/CustomerReport')

// 安装插件
Vue.use(Router)

const routes = [
    {
        path: '',
        redirect: '/employee',
    },
    {
        path: '/login',
        component: Login
    },
    {
        path: '',
        component: Home,
        children: [
            {
                path: '/employee',
                meta: {
                    name: '/employee',
                    title: '员工管理'
                },
                component: Employee
            },
            {
                path: '/department',
                meta: {
                    name: '/department',
                    title: '部门管理'
                },
                component: Department
            },
            {
                path: '/employee/add',
                meta: {
                    name: '/employee',
                    title: '员工新增'
                },
                component: AddEmployee
            },
            {
                path: '/employee/edit/:eid',
                meta: {
                    name: '/employee',
                    title: '员工编辑'
                },
                component: AddEmployee
            },
            {
                path: '/permission',
                meta: {
                    name: '/permission',
                    title: '权限管理'
                },
                component: Permission
            },
            {
                path: '/role',
                meta: {
                    name: '/role',
                    title: '角色管理'
                },
                component: Role
            },
            {
                path: '/role/add',
                meta: {
                    name: '/role',
                    title: '角色新增'
                },
                component: AddRole
            },
            {
                path: '/role/edit/:roleid',
                meta: {
                    name: '/role',
                    title: '角色编辑'
                },
                component: AddRole
            },
            {
                path: '/sysdictionary',
                meta: {
                    name: '/sysdictionary',
                    title: '数据字典管理'
                },
                component: SystemDictionary
            },
            {
                path: '/sysdictionaryitem',
                meta: {
                    name: '/sysdictionaryitem',
                    title: '数据字典明细管理'
                },
                component: SystemDictionaryItem
            },
            {
                path: '/customerlist',
                meta: {
                    name: '/customerlist',
                    title: '客户管理'
                },
                component: CustomerLists
            },
            {
                path: '/pcustomerlist',
                meta: {
                    name: '/pcustomerlist',
                    title: '潜在客户管理'
                },
                component: PCustomerLists
            },
            {
                path: '/customertracehistory',
                meta: {
                    name: '/customertracehistory',
                    title: '跟进历史'
                },
                component: CustomerTraceHistory
            },
            {
                path: '/rpoolcustomer',
                meta: {
                    name: '/rpoolcustomer',
                    title: '客户池管理'
                },
                component: RPoolCustomer
            },
            {
                path: '/failcustomer',
                meta: {
                    name: '/failcustomer',
                    title: '失败客户管理'
                },
                component: FailCustomer
            },
            {
                path: '/customerreport',
                meta: {
                    name: '/customerreport',
                    title: '潜在客户报表'
                },
                component: CustomerReport
            }
        ]
    },
    {
        path: "*",
        redirect: "/"
    }
]
// 创建router
const router = new Router({
    routes
})

router.beforeEach((to, from, next) => {
    //
    if (localStorage.getItem('token') && to.path === '/login') {
        next(from)
    } else if (!localStorage.getItem('token') && to.path !== '/login'){
        next('/login')
    } else {
        next()
    }
})

export default router