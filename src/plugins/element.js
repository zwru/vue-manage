import Vue from 'vue'
import {
    Button,
    Form,
    FormItem,
    Input,
    Message,
    Menu,
    MenuItem,
    Submenu,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    Table,
    TableColumn,
    Pagination,
    Dialog,
    MessageBox,
    Select,
    Option,
    Switch,
    Transfer,
    DatePicker,
    Upload,
    Loading
} from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(TableColumn)
Vue.use(Table)
Vue.use(DropdownMenu)
Vue.use(Pagination)
Vue.use(Dialog)
Vue.use(Select)
Vue.use(Option)
Vue.use(Switch)
Vue.use(Transfer)
Vue.use(DatePicker)
Vue.use(Upload)
Vue.use(Loading)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm