import request from './request.js'

const path = process.env.NODE_ENV === 'development' ? '/api/coding' : ''

// 登陆
export const loginApi = params => request.post(`${path}/tokens`, params)

// 注销登陆
export const logoutApi = params => request.delete(`${path}/tokens/${params.token}`)

// 获取部门列表
export const getDepartMentsApi = params => request.get(`${path}/departments`, {params})

// 添加单个部门
export const addDepartMentApi = params => request.post(`${path}/departments`, params)

// 删除单个部门
export const delDepartmentApi = params => request.delete(`${path}/departments/${params.id}`)

// 更新单个部门
export const updateDepartmentApi = params => request.put(`${path}/departments/${params.id}`, params)

// 获取员工列表
export const getEmployeesApi = params => request.get(`${path}/employees`, {params})

//获取市场专员列表
export const getMarketEmployeesApi = params => request.get(`${path}/employees/market`, {params})

// 获取权限管理
export const getPermissionsApi = params => request.get(`${path}/permissions`, {params})

// 删除单个权限
export const delPermissionsApi = params => request.delete(`${path}/permissions/${params.id}`)

// 加载权限
export const reloadPermissionsApi = params => request.post(`${path}/permissions`, params)

// 查询角色
export const getRolesApi = params => request.get(`${path}/roles`, {params})

// 添加角色
export const addRolesApi = params => request.post(`${path}/roles`, params)

// 删除角色
export const delRoleApi = params => request.delete(`${path}/roles/${params.id}`)

// 查询单个角色信息
export const getRoleApi = params => request.get(`${path}/roles/${params.id}`)

// 修改单个角色的信息
export const updateRoleApi = params => request.put(`${path}/roles/${params.id}`, params)

// 新增单个员工
export const addEmployeeApi = params => request.post(`${path}/employees`, params)

// 获取单个员工信息
export const getEmployeeApi = params => request.get(`${path}/employees/${params.id}`)

// 删除某个员工的信息
export const delEmployeeApi = params => request.delete(`${path}/employees/${params.id}`)

// 更新某个员工信息
export const updateEmployeeApi = params => request.put(`${path}/employees/${params.id}`, params)

// 删除多个员工信息
export const delEmployeesApi = params => request.delete(`${path}/employees`, {params})

// 获取数据字典列表
export const getSysDictionariesApi = params => request.get(`${path}/systemDictionaries`, {params})

// 添加数据字典列表
export const addSysDictionariesApi = params => request.post(`${path}/systemDictionaries`, params)

// 更新数据字典列表
export const updateSysDictionariesApi = params => request.put(`${path}/systemDictionaries/${params.id}`, params)

// 获取数据字典明细列表
export const getSysDictionaryItemsApi = params => request.get(`${path}/systemDictionaryItems`, {params})

// 添加数据字典列表
export const addSysDictionaryItemsApi = params => request.post(`${path}/systemDictionaryItems`, params)

// 更新数据字典列表
export const updateSysDictionaryItemsApi = params => request.put(`${path}/systemDictionaryItems/${params.id}`, params)

// 删除某个员工的信息
export const delSysDictionaryItemsApi = params => request.delete(`${path}/systemDictionaryItems/${params.id}`)

// 获取客户列表
export const getCustomersApi = params => request.get(`${path}/customers`, {params})

// 获取跟进历史管理
export const getCusTraceHistoryApi = params => request.get(`${path}/customerTraceHistories`, {params})

// 添加潜在客户
export const addPCustomerApi = params => request.post(`${path}/customers`, params)

// 更新数据字典列表
export const updatePCustomerApi = params => request.put(`${path}/customers/${params.id}`, params)

// 更新数据字典列表
export const patchPCustomerApi = params => request.patch(`${path}/customers/${params.id}`, params)

// 添加客户跟进记录
export const addTCustomerHistoryApi = params => request.post(`${path}/customerTraceHistories`, params)

// 更新客户跟进记录
export const updateTCustomerHistoryApi = params => request.put(`${path}/customerTraceHistories/${params.id}`, params)

// 更新客户移交管理
export const updateCustomerTransfersApi = params => request.post(`${path}/customerTransfers`, params)

// 吸纳客户
export const transferCustomerApi = params => request.post(`${path}/customerTransfers/customers`, params)

// 获取客户报表信息
export const getEReportListsApi = params => request.get(`${path}/customers/reports/list`, {params})

// 获取客户饼状图信息
export const getEReportPieListApi = params => request.get(`${path}/customers/reports/pie`, {params})

// 获取客户条形图信息
export const getReportBarApi = params => request.get(`${path}/customers/reports/bar`, {params})

// 导出员工信息
export const exportEmployeeXlsApi =  `${path}/employees/xlses`

// 导入员工信息
export const importEmployeeXlsApi = `${path}/employees/xlses`
