module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://192.168.113.117:8080',
                // target: 'http://127.0.0.1:8086',
                // changeOrigin: true,
                pathRewrite: {
                    '^/api/coding/': '/'
                }
            }
        }
    }
}